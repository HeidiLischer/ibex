#########
# README
#########

 CombineBayenvOutput
---------------------

by Heidi Lischer 2016, University of Zurich

Will write one file for each environmental variable with significant
SNPs (bayes factor >= bf in all runs). The format is as follwos:
SNPname bf_run1 rho_run1 rhoRank_run1 bf_run2 rho_run2 rhoRank_run2...

Usage:          CombineBayenvOutput <options...>

Options:
-? or -h        For this page.
-i              Comma separated list of Bayenv output files (file1,file2,...)
-o              Output file
-bf             Minimum bayes factor


How to run: java -jar CombineBayenvOutput.jar -h



 CorrectGenepop
----------------

by Heidi Lischer 2016, University of Zurich

Corrects GENEPOP files of Nemo (removes last 4 columns)
and allows to random subsample samples

Usage:          CorrectGenepop <options...>

Options:
-? or -h        For this page.
-i              Genepop input file
-o              Genepop output file
-n              Number of samples to random subsample (optional)
-rmPop          Comma separated list of populations to remove (1,3,10,...) (optional)
-x              Don't write file if empty populations exist
-addLoci        Additional file with loci to add (optional)
                  (1. line: header (comma separated list of loci names);
                   1. column: population; further columns: loci genotypes)


How to run: java -jar CorrectGenepop.jar -h